<?php

   /********** PHP INIT **********/
   header('Access-Control-Allow-Origin: *');  
   header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization');
   header('Cache-Control: no-cache');
   set_time_limit(0);
   ini_set('memory_limit', '-1');
   ini_set('mysql.connect_timeout','0');
   ini_set('max_execution_time', '0');
   ini_set('date.timezone', 'Asia/Manila');

   require_once (dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR  .'php'.DIRECTORY_SEPARATOR . 'api' . DIRECTORY_SEPARATOR . 'settings' . DIRECTORY_SEPARATOR . 'config.php');


   $url = '';
 
   $serialID = uniqid();

    // var_dump($_POST['module']);

   switch ($_POST['module']) { 

      case 'profile':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'profile' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      case 'vouchers':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'vouchers' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'vouchers' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      case 'posts':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      case 'brands':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'brands' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      case 'products':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      case 'levelcategories':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'levelcategories' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'levelcategories' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      case 'accounts':
         $url = dirname(dirname(__FILE__));
         $path_full= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'accounts' . DIRECTORY_SEPARATOR . 'full';
         $path_thumb= dirname(dirname(dirname(__FILE__))).DIRECTORY_SEPARATOR. 'assets' . DIRECTORY_SEPARATOR. 'images' . DIRECTORY_SEPARATOR . 'accounts' . DIRECTORY_SEPARATOR . 'thumb';
         break;

      default:
         # code...
         break;
   }  



	$width = 200;
	$height = 200;
	$counter = 0;
	$valid_formats = array("jpg", "jpeg", "png", "gif", "bmp", "JPG", "JPEG", "PNG", "GIF", "BMP");

	if (!file_exists($path_full)) {
      mkdir($path_full, 0777, true);
	}
	if (!file_exists($path_thumb)) {
      mkdir($path_thumb, 0777, true);
	}


	$file_name = 'file';
   $names = $_FILES[$file_name]['name'];
   $size = $_FILES[$file_name]['size'];
   $tmp  = $_FILES[$file_name]['tmp_name'];
   $type = $_FILES[$file_name]['type']; 
   

   if ($names) {
      if (strlen($names)){
         $counter += 1;
         $ext_arr = explode(".",$names);
         $ext = end($ext_arr);

         if (in_array($ext,$valid_formats)){

            if ($size<2e+6) {

               if (move_uploaded_file($tmp, $path_full.'/'.$serialID.'.'.$ext)) {
                  
                  list($width_orig, $height_orig) = getimagesize($path_full.'/'.$serialID.'.'.$ext);
                  $ratio_orig = $width_orig/$height_orig;
                  
                  //Height Driven
                  if ($width/$height < $ratio_orig) {
                     $width = $height*$ratio_orig;
                  } else {
                     $height = $width/$ratio_orig;
                  }

                  if ($ext == 'jpg' || $ext == 'JPG' || $ext == 'jpeg' || $ext == 'JPEG') {
                     $imgSrc = $path_full.'/'.$serialID.'.'.$ext;
                     $imgSrc = str_replace("../../../", $url, $imgSrc); 
                     $image = imagecreatefromjpeg($imgSrc);
                  } else if ($ext == 'png' || $ext == 'PNG') {
                     $imgSrc = $path_full.'/'.$serialID.'.'.$ext;
                     $imgSrc = str_replace("../../../", $url, $imgSrc);
                     $image = imagecreatefrompng($imgSrc);
                  }

                  $image_p = imagecreatetruecolor($width, $height);
                  $almostblack = imagecolorallocate($image_p,255,255,255);
                  imagefill($image_p,0,0,$almostblack);
                  $black = imagecolorallocate($image_p,0,0,0);
                  imagecolortransparent($image_p,$almostblack); 
                  imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                     
                  // Output
                  if (imagejpeg($image_p, $path_thumb.'/'.$serialID.'.'.$ext, 100)){
                     $path = str_replace('../../', '', $path_full);
                  } else { 
                     echo json_encode(array("response"=>"Failed", "description"=>"Thumb image not uploaded to server"));
                  }    

               } else { 
                  echo json_encode(array("response"=>"Failed", "description"=>"Full image not uploaded to server")); 
               }
            } 
            else {
               echo json_encode(array("response"=>"Failed", "description"=>"Exceeds required image size"));  
            }
         } else {
            echo json_encode(array("response"=>"Failed", "description"=>"Invalid format"));  
            exit;
         }
      } else {
         echo json_encode(array("response"=>"Failed", "description"=>"Invalid image"));  
         exit;
      }
   }
   else{
      echo json_encode(array("response"=>"Failed", "description"=>"Cannot find image"));
      exit;      
   }

   if (file_exists($path_full.'/'.$serialID.'.'.$ext) && file_exists($path_thumb.'/'.$serialID.'.'.$ext)) {
      echo json_encode(array("response"=>"Success", "data"=>$serialID.'.'.$ext, "path_full"=>$path_full.'/'.$serialID.'.'.$ext, "path_thumb"=>$path_thumb.'/'.$serialID.'.'.$ext ));
   }
   else{
      echo json_encode(array("response"=>"Failed", "description"=>"Image failed to create"));
      exit;
   }
    
?>