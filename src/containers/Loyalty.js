import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/loyalty/LoyaltyComponent';

class Loyalty extends Component {
  render() {
    const {actions, loyaltyState, router } = this.props;
    return <Main actions={actions} data={loyaltyState} router={router}/>;
  }
}

Loyalty.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { loyaltyState: state.loyaltyState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    loyaltyAction: require('../actions/loyaltyAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.loyaltyAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Loyalty);
