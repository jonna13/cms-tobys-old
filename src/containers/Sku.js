import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/sku/SkuComponent';

class Sku extends Component {
  render() {
    const {actions, skuState, router } = this.props;
    return <Main actions={actions} data={skuState} router={router}/>;
  }
}

Sku.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { skuState: state.skuState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    skuAction: require('../actions/skuAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.skuAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Sku);
