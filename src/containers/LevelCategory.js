import React, {
  Component,
  PropTypes
} from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Main from '../components/levelcategory/LevelCategoryComponent';

class LevelCategory extends Component {
  render() {
    const {actions, levelCategoryState, router } = this.props;
    return <Main actions={actions} data={levelCategoryState} router={router}/>;
  }
}

LevelCategory.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  const props = { levelCategoryState: state.levelCategoryState };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    levelCategoryAction: require('../actions/levelCategoryAction.js')
  };
  const actionMap = { actions: bindActionCreators(actions.levelCategoryAction, dispatch) };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LevelCategory);
