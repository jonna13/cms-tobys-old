/**
 * Created by jedachas on 3/6/17.
 *
 * Loading Reducers
 *
 */
import * as types from '../constants/LoadingActionTypes';
import update from 'react-addons-update';

const initialState = {
  loading: {
    show: false,
  }
};

module.exports = function(state = initialState, action){
  switch (action.type) {

    case types.SHOW_LOADING:
      return update(state, {
        loading: {
          show: { $set: true }
        }
      });
      break;

    case types.HIDE_LOADING:
      return update(state, {
        loading: {
          show: { $set: false }
        }
      })

    default:
      return state;
  }
}
