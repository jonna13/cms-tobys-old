/**
 * Created by jedachas on 3/8/17.
 */
import * as types from '../constants/LevelCategoryActionTypes';
import update from 'react-addons-update';

const initialState = {
  levelcategories: {
    records: [],
    pageinfo: {
      pageSize: 0,
      totalPage: 0,
      sortColumnName: null,
      sortOrder: 'DESC',
      currentPage: 1,
      pageSize: 5,
      searchFilter: '',
      totalRecord: 0,
      selectedStatus: null
    }
  },
  selectedRecord: {},
}

module.exports = function(state = initialState, action) {
  switch (action.type) {
    case types.VIEW_LEVELCATEGORY_SUCCESS:
      return update(state, {
        selectedRecord: {
          $set: action.data.data
        }
      });

    case types.REMOVE_SELECTED_RECORD:
      return update(state, {
        selectedRecord: { $set: [] }
      });

    case types.CHANGE_FILTER_STATUS:
      return update(state, {
        levelcategories: {
          pageinfo : {
            selectedStatus : { $set: action.status }
          }
        }
      });

    case types.GET_LEVELCATEGORY_SUCCESS:
      console.info('GET_LEVELCATEGORY_SUCCESS', action.data);
      return update(state, {
        levelcategories: {
          records: {
            $set: action.data.data
          },
          pageinfo: {
            $set: action.data.pageinfo
          }
        }
      });

    default:
      return state;

  }
}
