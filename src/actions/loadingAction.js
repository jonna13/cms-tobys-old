/**
 * Created by jedachas on 3/1/17.
 *
 * Loading Action
 *
 */

 import * as types from '../constants/LoadingActionTypes';

 export const showLoading = () => {
   return{
     type: types.SHOW_LOADING
   }
 }

export const hideLoading = () => {
  return{
    type: types.HIDE_LOADING
  }
}
