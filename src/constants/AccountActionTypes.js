/**
 * Created by jedachas on 3/29/17.
 */
export const VIEW_ACCOUNT_SUCCESS = 'VIEW_ACCOUNT_SUCCESS';
export const GET_ACCOUNT_SUCCESS = 'GET_ACCOUNT_SUCCESS';

export const CHANGE_FILTER_STATUS = 'CHANGE_FILTER_STATUS';
export const REMOVE_SELECTED_RECORD = 'REMOVE_SELECTED_RECORD';

export const GET_CATEGORY_SUCCESS = 'GET_CATEGORY_SUCCESS';
export const GET_LOCATIONSCATEGORY_SUCCESS = 'GET_LOCATIONSCATEGORY_SUCCESS';

export const GET_ACCOUNTLOCATION_SUCCESS = 'GET_ACCOUNTLOCATION_SUCCESS';
export const GET_ACCOUNTLOCATION_FAILED = 'GET_ACCOUNTLOCATION_FAILED';
export const GET_ACCOUNTBRAND_SUCCESS = 'GET_ACCOUNTBRAND_SUCCESS';
export const GET_ACCOUNTBRAND_FAILED = 'GET_ACCOUNTBRAND_FAILED';
