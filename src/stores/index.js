import { createStore, applyMiddleware, compose } from 'redux';
const reducers = require('../reducers');
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';

// import { createHistory, useBasename } from 'history';
import { hashHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import Config from '../config/base';

const logger = createLogger();

// const browserHistory = useBasename(createHistory)({
//     basename: Config.PROJECT_BASE
// });
// const browserHistory = useRouterHistory(createHistory)({
//   basename: Config.PROJECT_BASE
// });
const router = routerMiddleware(hashHistory);

const enhancer = compose(
  applyMiddleware(thunk, router),
  window.devToolsExtension ? window.devToolsExtension() : f => f
);

// module.exports = function(initialState) {
export default function configureStore(initialState) {

  const store = createStore(
    reducers,
    initialState,
    enhancer
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers')
      store.replaceReducer(nextReducer)
    })
  }

  return store
}
