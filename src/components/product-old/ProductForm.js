/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import { DefaultTextArea } from '../common/fields';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';

class ProductForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      category: '',
      subcategory: '',
      shouldDisplay: false,
    };
  }

  componentWillMount(){
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      this.setState({
        category: nextProps.data.selectedRecord.categoryID,
        subcategory: nextProps.data.selectedRecord.subcategoryID
      });
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({shouldDisplay: true});
      }
    }
  }

  handleCategoryChange = (e, idx, category) => {
    this.setState({category});
    this.setState({subcategory:''});
    this.props.onCategoryChange(category);
  };

  handleSubcategoryChange = (e, idx, subcategory) => {
    this.setState({subcategory});
    this.props.onSubCategoryChange(subcategory);
  };

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    let categoryList = [],
        subcategoryList = [];
    const {data} = this.props;

    if (this.props.data.categories) {
      categoryList.push(<MenuItem key={0} value='' primaryText='Select Category' />);
      this.props.data.categories.map( (val, idx) => {
        categoryList.push(<MenuItem key={(idx+1)} value={val.categoryID} primaryText={val.name} />);
      });
    }

    if (this.props.data.subcategories) {
      subcategoryList.push(<MenuItem key={0} value='' primaryText='Select Subcategory' />);
      this.props.data.subcategories.map( (val, idx) => {
          subcategoryList.push(<MenuItem key={(idx+1)} value={val.subcategoryID} primaryText={val.name} />);
      });
    }

    if (this.state.shouldDisplay){
      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="name"
                  hintText="Enter Product Name"
                  floatingLabelText="Product Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="price"
                  hintText="Enter Price"
                  floatingLabelText="Price"
                  defaultValue={(data.selectedRecord.price) ? data.selectedRecord.price : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                  type="number"
                /><br />
                <SelectField value={this.state.category} onChange={this.handleCategoryChange}
                  className='textfield-regular' floatingLabelText="Brand" hintText="Select Brand">
                  <MenuItem value="discount" primaryText="Discount" />
                  <MenuItem value="benefits" primaryText="Benefits" />
                  <MenuItem value="birthday" primaryText="Birthday" />
                  <MenuItem value="wemissyou" primaryText="We Miss You" />
                </SelectField><br />
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                {/*<DropDownMenu value={this.state.category} onChange={this.handleCategoryChange}
                  autoWidth={false} className='dropdownButton'>
                  {categoryList}
                </DropDownMenu>
                <br />*/}
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default ProductForm;
