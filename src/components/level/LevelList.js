/**
 * Created by jedachas on 3/9/17.
 */
import React, {Component} from 'react';
import ListTable from '../common/table/ListTableComponent';
import Config from '../../config/base';
import Enlist from '../common/EnlistComponent';
import ScreenListener from '../common/ScreenListener';

import GridRow from './LevelGridRow';

class LevelList extends Component {
  state = {
    screenWidth: window.innerWidth
  }

  handleScreenChange = (w, h) => {
    this.setState({screenWidth: w});
  }

  handleGetData = (params) => {
    this.props.actions.getLevels(params);
  }

  render(){
    let {levels} = this.props.data;
    const headers = [
      { title: '', value: ''},
      { title: 'Name', value: 'name'},
      { title: 'Min', value: 'min'},
      { title: 'Max', value: 'max'},
      { title: 'Level', value: 'levels'},
      { title: 'Status', value: 'status'}
    ];

    var rows = [];
    if (levels.records.length > 0) {
      levels.records.forEach((val, key) => {
        rows.push(<GridRow
          key={key}
          items={val}
          onEditClick={this.props.onEdit}/>);
      })
    }

    return(
      <div className='content-container'>
        { (this.state.screenWidth < 761) ?
          <Enlist
            data={this.props.data.levels}
            onGetData={this.handleGetData}
            hasImage={false}
            onEditClick={this.props.onEdit} />
          :
          <ListTable
            headers={headers}
            data={this.props.data.levels}
            onGetData={this.handleGetData}>
              {rows}
          </ListTable>
        }
        <ScreenListener onScreenChange={this.handleScreenChange}/>
      </div>
    );
  }

}

export default LevelList;
