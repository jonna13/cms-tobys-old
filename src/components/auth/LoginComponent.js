'use strict';

import React from 'react';
import TextField from 'material-ui/TextField';
import Config from '../../config/base';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';

import { Grid, Col, Row, Panel, FormControl, ControlLabel, InputGroup } from 'react-bootstrap';

import AuthApi from '../../api/auth.api';
const logo = require('../../images/login-logo.png');

class LoginComponent extends React.Component {

    constructor(props){
      super(props);

      this.state = {
        credentials: {
          username: '',
          password: ''
        }
      }
    }

    handleInput = (event) => {
      const field = event.target.name;
      const details = this.state.credentials;
      details[field] = event.target.value;
    }

    onLogin = (event) => {
      console.info('onLogin', this.state);
      const { actions } = this.props;
      actions.loginUser(this.state.credentials, this.props.router);
    }

    handleKeyPress = (event) => {
      if(event.key == 'Enter') {
         this.onLogin();
      }
    }

    render() {
      return (
        <div className="login">
          <Grid>
            <Row>
              <Col xs={12} md={4} lg={4} mdOffset={4} lgOffset={4}>
                <div className='login-frm'>
                  <center><img src={logo} className='login-img'/></center>
                  <Panel className='login-form'>
                    <center><h3>Login to your account</h3></center>
                    <br />
                      <FormControl
                        name="username"
                        placeholder="Username"
                        type="text"
                        defaultValue={this.state.credentials.username}
                        onChange={this.handleInput}
                        onKeyPress={this.handleKeyPress}
                        className='input-lg'
                      />
                    <br />
                      <FormControl
                        name="password"
                        placeholder="Password"
                        type="password"
                        defaultValue={this.state.credentials.password}
                        onChange={this.handleInput}
                        onKeyPress={this.handleKeyPress}
                        className='input-lg'
                      />

                    <br />

                    <RaisedButton
                      label="Login"
                      primary={true}
                      onClick={this.onLogin}
                      className="login-button"
                      style={{ float: 'right'}}
                    />
                  </Panel>
                </div>
              </Col>
            </Row>
          </Grid>
        </div>

      );
    }

}

LoginComponent.displayName = 'AuthLoginComponent';

// Uncomment properties you need
// LoginComponent.propTypes = {};
// LoginComponent.defaultProps = {};

export default LoginComponent;
