/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import Config from '../../config/base';
import TextField from 'material-ui/TextField';
import { DefaultTextArea } from '../common/fields';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem'
import { Grid, Row, Col } from 'react-bootstrap';
import AppStyle from '../../styles/Style.js';
import ImageUpload from '../common/ImageUpload';


class ProductForm extends React.Component{

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      brandID: ''
    };
  }

  componentWillMount(){
    const {actions} = this.props;
    // console.log('actions', actions);
    actions.getCategory();
    // for add
    if (!this.props.shouldEdit){
      this.setState({shouldDisplay: true});
    }
  }

  componentWillReceiveProps(nextProps){
    // for edit
    if (nextProps.shouldEdit) {
      if (!_.isEmpty(nextProps.data.selectedRecord)) {
        this.setState({
          shouldDisplay: true
        });
      }
    }
  }

  handleBrandChange = (e, idx, brandID) => {
    this.setState({brandID});
    this.props.onChangeBrand(brandID);
  }

  onCheck = (e, val) =>{
    this.props.onStatusChange(val);
  }

  render(){
    const {data} = this.props;
    {/*console.log('product data', data);*/}

    var category = [];
        if (data.products.category.length > 0) {
          data.products.category.forEach((val, key) => {
            category.push(
              <MenuItem
              key={key}
              value={val.brandID}
              primaryText={val.name}
              />
            );
          });
        }

    if (this.state.shouldDisplay){

      return(
        <Grid fluid>
          <Row>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-container'>
                <div className='item-image-box'>
                  <ImageUpload image={null}
                    onImageChange={this.props.onImageChange}
                    image={(data.selectedRecord.image) ? data.selectedRecord.image : '' }
                    imageModule={this.props.imageModule}
                    info="Ratio 5:4 (ex: 1000x800 pixels). Max 2MB"/>
                </div>
              </div>
            </Col>
            <Col xs={12} sm={12} md={6} lg={6}>
              <div className='content-field-holder'>
                <TextField
                  name="name"
                  hintText="Enter Product Name"
                  floatingLabelText="Product Name"
                  defaultValue={(data.selectedRecord.name) ? data.selectedRecord.name : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                <TextField
                  name="price"
                  hintText="Enter Price"
                  floatingLabelText="Price"
                  defaultValue={(data.selectedRecord.price) ? data.selectedRecord.price : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                  type="number"
                  min="0"
                /><br />
                {/*<SelectField
                  value={(data.selectedRecord.brandID) ? data.selectedRecord.brandID: this.state.brandID}
                  onChange={this.handleBrandChange}
                  className='textfield-regular'
                  floatingLabelText="Brand"
                  hintText="Select Brand">
                  {category}
                </SelectField><br />*/}
                <DefaultTextArea
                  name="description"
                  hintText="Enter Description"
                  floatingLabelText="Description"
                  defaultValue={(data.selectedRecord.description) ? data.selectedRecord.description : '' }
                  onChange={this.props.onChange}
                  rows={4}
                  maxSize={Config.FIELD_EDIT.PRIMARY_MAX_SIZE}
                  regular={true}
                  /><br />
                <TextField
                  name="url"
                  hintText="Enter URL"
                  floatingLabelText="URL"
                  defaultValue={(data.selectedRecord.url) ? data.selectedRecord.url : '' }
                  onChange={this.props.onChange}
                  className="textfield-regular"
                /><br />
                {/*<DropDownMenu value={this.state.category} onChange={this.handleCategoryChange}
                  autoWidth={false} className='dropdownButton'>
                  {categoryList}
                </DropDownMenu>
                <br />*/}
                <Checkbox
                  label="Status"
                  className='checkBox'
                  defaultChecked={ (this.props.shouldEdit) ? ( (data.selectedRecord.status == 'active') ? true : false) : false }
                  onCheck={this.onCheck}
                />
              </div>
            </Col>
          </Row>
        </Grid>
      );
    }
    else{
      return null;
    }



  }
}

export default ProductForm;
