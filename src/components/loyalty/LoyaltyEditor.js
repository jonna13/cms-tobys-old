/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import LoyaltyForm from './LoyaltyForm';


class LoyaltyEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loyalty: {
        levelID: '',
        name: '',
        points: 0,
        promo: '',
        terms: '',
        description: '',
        status: false,
        image: ''
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewLoyalty(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.loyaltyState.selectedRecord)) {
        this.setState({
          loyalty: nextProps.loyaltyState.selectedRecord
        });
      }
    }
  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/loyalty');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput(this.state.loyalty)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_LOYALTY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateLoyalty(this.state.loyalty, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.loyalty)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_LOYALTY_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addLoyalty(this.state.loyalty, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let loyalty = this.state.loyalty;
    loyalty[field] = e.target.value;
  }

  validateInput = (data) => {
    const {dialogActions} = this.props;
    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    } 
    if (data.points == 0) {
      dialogActions.openNotification('Oops! No points found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.loyalty['status'] = e;
  }

  // handle level
  handleLevelChange = (e) => {
    console.info(e);
    this.state.loyalty['levelID'] = e;
  }

  render(){
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Campaign' : 'Add Campaign'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <LoyaltyForm
          data={this.props.loyaltyState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          onLevelChange={this.handleLevelChange}
          onStatusChange={this.handleStatusChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    loyaltyState: state.loyaltyState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    loyaltyAction: require('../../actions/loyaltyAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.loyaltyAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LoyaltyEditor);
