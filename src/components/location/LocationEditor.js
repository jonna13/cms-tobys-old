/**
 * Created by jedachas on 2/23/17.
 */
import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Divider from 'material-ui/Divider';
import {UpdateButton, ReturnButton} from '../common/buttons';
import Config from '../../config/base';
import _ from 'lodash';
import update from 'react-addons-update';
import {EmailIsValid} from '../common/Utility';

import LocationForm from './LocationForm';


class LocationEditor extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      location: {
        brandID: '',
        name: '',
        address: '',
        latitude: '',
        longitude: '',
        businessHrs: '',
        branchCode: '',
        phone: '',
        email: '',
        status: false,
        locFlag: false
      }
    }
  }

  componentWillMount(){
    const {actions} = this.props;
    if (this.props.params.id) {
      actions.viewLocation(this.props.params.id);
    }
    actions.getBranchCodes();
    console.log('actions', actions);
  }

  componentWillReceiveProps(nextProps){
    if (nextProps.params.id) {
      if (!_.isEmpty(nextProps.locationState.selectedRecord)) {
        this.setState({
          location: nextProps.locationState.selectedRecord
        });
      }
    }else{
      var bID = nextProps.locationState.locations.category.map(function(x) {
         return x['brandID'];
      });

      // var myJSON = JSON.stringify(bID[0]);
      console.log('bID', bID[0]);
      let _location = this.state.location;
      _location.brandID = bID[0];
      this.setState({
        location: _location
      });
    }


  }

  // handle going back
  handleReturn = () => {
    this.props.router.push('/location');
  }

  componentWillUnmount(){
    const {actions} = this.props;
    actions.removeSelectedRecord();
  }

  // handle Update
  handleUpdate = () => {
    const {dialogActions} = this.props;
    if (this.validateInput2(this.state.location)) {
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.UPDATE_LOCATION_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.updateLocation(this.state.location, this.props.router);
          }
        });
    }
  }

  // handle Add
  handleAdd = () => {
    if (this.validateInput(this.state.location)) {
    // if (this.validateBranchCode(this.state.location)) {
      const {dialogActions} = this.props;
      dialogActions.openConfirm(Config.DIALOG_MESSAGE.CONFIRM_TITLE,
        Config.DIALOG_MESSAGE.ADD_LOCATION_MESSAGE,
        Config.DIALOG_MESSAGE.CONFIRM_LABEL,
        Config.DIALOG_MESSAGE.CLOSE_LABEL,
        (result) => {
          if (result) {
            let {actions} = this.props;
            actions.addLocation(this.state.location, this.props.router);
          }
      });
    }
  }

  // handle input data change
  handleData = (e, idx, val) => {
    let field = e.target.name;
    let location = this.state.location;
    location[field] = e.target.value;
  }

  // validateBranchCode = (data) => {
  //   const {dialogActions} = this.props;
  //   let branchArray = this.props.locationState.branchCodes;
  //   console.log('Validate Branch Code', branchArray);
  //
  //   function in_array(array, branchCode)
  //   {
  //       return array.some(function(item) {
  //           return item.branchCode === branchCode;
  //       });
  //   }
  //
  //   var brCode = data.branchCode;
  //   let userInput = brCode.toString();
  //   console.log('User Input', userInput);
  //
  //   if (in_array(branchArray, userInput) == true) {
  //     dialogActions.openNotification('Oops! Branch Code Already Used!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
  //     return false;
  //   }
  //
  //   return true;
  // }


  validateInput = (data) => {
    const {dialogActions} = this.props;
    let branchArray = this.props.locationState.branchCodes;
    // console.log('Validate Branch Code', branchArray);

    // check if Branch Code already exists
    function in_array(array, branchCode)
    {
        return array.some(function(item) {
            return item.branchCode === branchCode;
        });
    }
    // check user input (Branch Code)
    var brCode = data.branchCode;
    let userInput = brCode.toString();
    // console.log('User Input', userInput);

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.address == '') {
      dialogActions.openNotification('Oops! No address found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.email != '') {
      if (!EmailIsValid(data.email)) {
        dialogActions.openNotification('Oops! Email is not valid', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }
    if (in_array(branchArray, userInput) == true) {
      dialogActions.openNotification('Oops! Branch Code Already Used!', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }

    return true;
  }

  // Validation for Update button
  validateInput2 = (data) => {
    const {dialogActions} = this.props;
    let branchArray = this.props.locationState.branchCodes;
    // console.log('Validate Branch Code', branchArray);

    // check if Branch Code already exists
    function in_array(array, branchCode)
    {
        return array.some(function(item) {
            return item.branchCode === branchCode;
        });
    }
    // check user input (Branch Code)
    var brCode = data.branchCode;
    let userInput = brCode.toString();
    // console.log('User Input', userInput);

    if (data.name == '') {
      dialogActions.openNotification('Oops! No name found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.address == '') {
      dialogActions.openNotification('Oops! No address found', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
      return false;
    }
    if (data.email != '') {
      if (!EmailIsValid(data.email)) {
        dialogActions.openNotification('Oops! Email is not valid', Config.DIALOG_MESSAGE.NOTIFICATION_DELAY);
        return false;
      }
    }

    return true;
  }

  // handle Branch Code
  handleBranchCodeChange = (value) => {
    this.state.location['branchCode'] = value;
  }

  // handle status locFlag
  handleLocFlagChange = (e) => {
    this.state.location['locFlag'] = e;
  }

  // handle status checkbox
  handleStatusChange = (e) => {
    this.state.location['status'] = e;
  }

  handleChangeBrand = (e) => {
    this.state.location['brandID'] = e;
  }



  render(){
    const {actions} = this.props;
    console.log('nram', this.state.location);
    return(
      <div>
        <h2 className='content-heading'> { (this.props.params.id) ? 'Edit Location' : 'Add Location'}</h2>
        <Divider className='content-divider' />

        { /* Action Buttons */ }
        <ReturnButton handleOpen={this.handleReturn}/>
        { (this.props.params.id) ? <UpdateButton handleOpen={this.handleUpdate}/> : <UpdateButton handleOpen={this.handleAdd}/> }

        { /* Form */ }
        <LocationForm
          data={this.props.locationState}
          shouldEdit={ (this.props.params.id) ? true : false }
          onChange={this.handleData}
          actions={actions}
          onBranchCodeChange={this.handleBranchCodeChange}
          onChangeBrand={this.handleChangeBrand}
          onStatusChange={this.handleStatusChange}
          onLocFlagChange={this.handleLocFlagChange}
          />

      </div>
    );
  }
}


function mapStateToProps(state) {
  const props = {
    locationState: state.locationState,
    dialogState: state.dialogState
  };
  return props;
}

function mapDispatchToProps(dispatch) {
  const actions = {
    locationAction: require('../../actions/locationAction.js'),
    dialogAction: require('../../actions/dialogAction.js')
  };
  const actionMap = {
    actions: bindActionCreators(actions.locationAction, dispatch),
    dialogActions: bindActionCreators(actions.dialogAction, dispatch),
  };
  return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationEditor);
