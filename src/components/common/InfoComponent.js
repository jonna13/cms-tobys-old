'use strict';

import React, {Component} from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import Config from '../../config/base';

class InfoComponent extends Component {
  render(){
    const actions = [
      <FlatButton
        label="Ok"
        primary={true}
        keyboardFocused={true}
        onTouchTap={this.props.onRequestClose}
      />
    ];
    let d = new Date();
    let date = d.getFullYear()+'.'+(d.getMonth()+1)+'.'+d.getDate()+'.'+
      d.getHours()+'.'+d.getMinutes()+'.'+d.getSeconds();

    const HeaderTitle = (
      <div>
        {Config.BUILD_NAME + ' ' + Config.BUILD_VERSION}
        <label className='info-build'>build {date}</label>
      </div>
    );

    return(
      <div>
        <Dialog
          title={HeaderTitle}
          actions={actions}
          modal={false}
          open={this.props.open}
          onRequestClose={this.props.onRequestClose}
        >
          <div className="info-container">
            <label>Copyright © 2017. Appsolutely Inc.
              <br />Unit-1202, 12th Floor, AIC Burgundy Tower, ADB Avenue,
              <br />Ortigas Center Pasig City, Philippines, 1605
              <br /><br />www.appsolutely.ph
              <br />(+632) 584-9411
              <br />(+632) 215-4156
            </label>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default InfoComponent;
