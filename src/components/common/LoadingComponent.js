'use strict';

import React from 'react';
import RefreshIndicator from 'material-ui/RefreshIndicator';

class LoadingComponent extends React.Component {

  render() {
    return(
      <div className={ (this.props.show) ? 'refresh-loading-container' : 'hide-element'}>
        <div className={ (this.props.show) ? 'refresh-loader' : 'hide-element'}>
          <RefreshIndicator
            size={50}
            left={0}
            top={0}
            status={(this.props.show) ? 'loading' : 'hide'}
          />
        </div>
      </div>
    );
  }
}

export default LoadingComponent;
