/**
 * Created by jedachas on 3/28/17.
 */
 'use strict';

 import React, {Component} from 'react';

 class ScreenListener extends Component {

   componentDidMount(){
     window.addEventListener('resize', this.updateDimensions);
   }

   componentWillUnmount() {
     window.removeEventListener('resize', this.updateDimensions);
   }

   updateDimensions = () => {
     let width = window.innerWidth, height = window.innerHeight;
     if (this.props.onScreenChange) {
       this.props.onScreenChange(width, height);
     }
   }

   render(){
     return null;
   }

 }

export default ScreenListener;
